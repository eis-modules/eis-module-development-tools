module.exports = {
    config: {
        routeRoot: 'eis',
        asRouteService: true
    },
    i18n: {
        'en-us': {
            'module-title': 'EIS Development tools',
            'module-description': 'The tools which only should be used in development environment.'
        },
        'zh-cn':{
            'module-title': 'EIS开发工具',
            'module-description': '只能在开发环境中使用的一组工具。'
        }
    }
}