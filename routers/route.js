if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'bundle') {
    const path = require('path');
    const express = require(path.resolve('./') + "/node_modules/express");
    const router = express.Router();

    router.post('/ensure_eis_modules', async (req, res) => {
        req.body.modules = (req.body.modules || '').split(',') || [];

        res.app.logger.debug('Checking modules: ' + req.body.modules)
        const missingModules = [];

        const appModules = req.app.moduleNames || [];

        req.body.modules.forEach(m => {
            if (m && appModules.indexOf(m.trim()) < 0) missingModules.push(m);
        });

        if (missingModules.length <= 0)
            res.endWithData();
        else
            await res.endWithErr(500, `Missing modules: [${missingModules}]`);
        return;
    })

    router.post('/restart', () => {
        process.exit(1);
    })

    module.exports = router;
}